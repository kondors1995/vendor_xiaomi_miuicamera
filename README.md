# Leica Camera 5.0 for Xiaomi Mi 9 Series (davinci/raphael/cepheus) AOSP

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/ItzDFPlayer/vendor_davinci-miuicamera -b leica-5.0 vendor/xiaomi/miuicamera
```

Make these changes in **sm6150-common**

**sm6150.mk**
```
# MiuiCamera
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```
## Credits

### Original mod - https://github.com/a406010503/Miui_Camera

## Support

### https://t.me/itzdfplayer_stash <br>

